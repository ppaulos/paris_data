import logging

from data_core.file_loader import load_df, FileType
from data_core.processor import Processor, execution_time
from pyspark.sql import functions as F, DataFrame, Window


class VelibProcess(Processor):

    def __init__(self):
        super().__init__()

    @execution_time
    def process(self):
        logging.info('Loading velib disponibilite...')
        velib_df = load_df(self.spark, 'resources/velib-disponibilite-en-temps-reel.csv',
                            options={'header': 'true', 'delimiter': ';'},
                            file_type=FileType.CSV)

        output_velib_df = compute_pourcentage(velib_df, "Nombre total vélos disponibles", "Capacité de la station",
                                              output_column="dispo_station_pourcentage")

        window_spec = Window.partitionBy()

        output_velib_df = output_velib_df.withColumn("rang_disponibilite",
                                                     F.row_number().over(
                                                         window_spec.orderBy(F.col("dispo_station_pourcentage").desc())))

        output_velib_df = output_velib_df \
            .withColumn("disponibilite_globale", F.sum("Nombre total vélos disponibles").over(window_spec)) \
            .withColumn("capacite_globale", F.sum("Capacité de la station").over(window_spec))

        output_velib_df = output_velib_df.withColumn("disponibilite_moyenne",
                                                     F.avg("dispo_station_pourcentage").over(window_spec))

        output_velib_df.write.mode("overwrite").parquet("/tmp/output_velib")


def compute_pourcentage(df: DataFrame, value_column, total_column, output_column="pourcentage") -> DataFrame:
    return df.withColumn(output_column, (df[value_column] / df[total_column] * 100).cast("int"))
