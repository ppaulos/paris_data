from data_core.runner import run
from paris_data.velib_process import VelibProcess

if __name__ == '__main__':
    run(VelibProcess())
