import logging

from data_core.file_loader import load_df, FileType
from data_core.processor import Processor, execution_time


class ArbresProcess(Processor):

    def __init__(self):
        super().__init__()

    @execution_time
    def process(self):
        logging.info('Loading arbres plantés ...')
        arbres_df = load_df(self.spark, 'resources/les-arbres-plantes.csv',
                            options={'header': 'true', 'delimiter': ';'},
                            file_type=FileType.CSV)
        arbres_df.show(truncate=False)
