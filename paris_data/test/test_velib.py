from paris_data.test.core_test import CoreTest
from paris_data.velib_process import compute_pourcentage


class VelibTest(CoreTest):
    def test_compute_pourcentage(self):
        data_list = ([1, 2], [1, 1])
        columns = ["value", "total"]
        data_df = self.spark.createDataFrame(data_list, columns)
        result_df = compute_pourcentage(data_df, "value", "total")
        result_list = result_df.rdd.map(lambda x: x["pourcentage"]).collect()
        expected_results = [50, 100]
        self.assertCountEqual(result_list, expected_results)

