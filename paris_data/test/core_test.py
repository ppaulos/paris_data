from unittest import TestCase

from pyspark.sql import SparkSession


class CoreTest(TestCase):
    def setUp(self) -> None:
        self.spark = SparkSession.builder.master("local[*]") \
            .appName("unittest") \
            .getOrCreate()
